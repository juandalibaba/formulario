import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Hero } from "../hero";

@Component({
  selector: "app-hero-form",
  templateUrl: "./hero-form.component.html",
  styleUrls: ["./hero-form.component.css"]
})
export class HeroFormComponent implements OnInit {
  hero: Hero;
  submitted = false
  @ViewChild('heroForm') heroForm: ElementRef
  @ViewChild('name') name: ElementRef

  powers = ["Really Smart", "Super Flexible", "Super Hot", "Weather Changer"];

  constructor() {
    this.hero = new Hero();
    this.hero.name = "Dr IQ";
    this.hero.alterEgo = "Chuck Overstreet";
    this.hero.power = this.powers[0];
  }

  submitForm(){
    console.log(this.heroForm)
    console.log(this.name)
    console.log(this.hero)
    setTimeout( () => {
      this.submitted = false
    }, 2000)
    this.submitted = true
  }

  ngOnInit() {}
}
